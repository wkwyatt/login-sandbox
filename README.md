# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This is a sandbox app for login with SAML Authentication
* Version
0.0.1


### How do I get set up? ###

* Summary of set up
Run these commands after downloading 
`npm install`
`gulp babel`
`ionic run ios --target="iPad-Air"`

* Configuration
cordova version 5.4.1
ionic version 1.7.14
node version 4.4.1


Warning, a Cordova or Node version that is too high caused the app to fail when running on device. Downgrading to 5.4.1 helped.

* Deployment instructions
`ionic run ios --target="iPad-Air"`

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact