angular.module('login-sandbox').controller('devController', [
	'$scope',
	'$http',
	'$q',
	'$sce',
	'$compile',
	'RESTService',
	function($scope, $http, $q, $sce, $compile, RESTService) {

		$scope.apiResponse = "<br /><br />...WAITING FOR RESPONSE";

		$scope.showResponse = (title, data) => {
			$scope.apiResponse += "<br /><br />" + title + "<br />" + data;
		}

		$scope.getAssertion = (username, password)=> {
			console.log("user: ", username);
			console.log("password: ", password);

			function removeBackslash(wordToParse) {
				return wordToParse.replace(/\"/g, "'");
			}

			function removeSpaces(wordToParse) {
				return wordToParse.replace(/\s+/g, '');
			}

			function b64encode(unencoded){
				// return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(unencoded));
				var wordArray = CryptoJS.enc.Utf8.parse(unencoded);
				var base64 = CryptoJS.enc.Base64.stringify(wordArray);
				return base64;
			}

			function b64decode(encoded) {
				// return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Base64.parse(encoded));
				var words = removeSpaces(encoded);
				words = CryptoJS.enc.Base64.parse(words);
				var parsedStr = words.toString(CryptoJS.enc.Utf8);
				// var parsedStr = CryptoJS.enc.Utf8.stringify(words);
				return parsedStr;
			};

			function xmlToJSON(data) {
                // convert the data to JSON and provide
                // it to the success function below
                var x2js = new X2JS();
                var json = x2js.xml_str2json( data );
                return json;
            }

			function getSAMLResponse(item) {
				return item.attributes['']
			}
			//*** base64 encode username:password
			var basicAuth = username + ':' + password;
			var basicAuthHeader = 'Basic ' + b64encode(basicAuth);

			let config = {
				xhrFields: {
		    	    withCredentials: true
		    	},
		    	headers: {
		    	    'Authorization': basicAuthHeader
		    	},
		    	followRedirect: true,
    			jar: true
		    };

			$http.get(
				"https://fed.testko.com/gmpmobile", 
				config, 
				{
					transformResponse: function(data) {
                      // convert the data to JSON and provide
                      // it to the success function below
                        var x2js = new X2JS();
                        var json = x2js.xml_str2json( data );
                        return json;
                    }
                })
				.then((response, status) => {
					console.log("THE RESPONSE: ", response);
					console.log("THE STATUS: ", status);
					
					$scope.showResponse("** SAML Login Response **", response.data);
					// var x2js = new X2JS();
					// console.log("X2JS: ", x2js);
					// console.log("function parser: ", x2js.xml_str2json(response.data));
					// var aftCnv = x2js.xml_str2json(response.data);
					// console.log("new plugin: ", aftCnv);
					// document.ready
					$scope.parsedResponse = response.data;


					console.log("RESPONSE DATA: ", $scope.parsedResponse);
					$scope.showResponse("** SAML Login Response Data **", response.data);
				    var parent = angular.element($scope.parsedResponse);
				    console.log("ANG ELEM(PARENT) OF RESPONSE DATA: ", parent);

				    angular.forEach(parent, function(value) {
				        if(angular.element(value).attr('action')) {
				            var samlURL = angular.element(value).attr('action');

				            console.log('found saml URL!!! - ', samlURL);
				            $scope.showResponse("** Action URI for SAML Assertion **", samlURL);
				            
				            angular.forEach(parent.children(), function(childValue) {
						        if(angular.element(childValue).attr('name') === 'SAMLResponse') {
						            var samlAssertion = angular.element(childValue).attr('value')

						            // Show SAML Assertion data
						            console.log('found the samlAssertion!!! - ', samlAssertion);
						            $scope.showResponse("** SAML Assertion **", samlAssertion);
						            console.log("DECODED SAML RESPONSE: ", { saml: b64decode(samlAssertion) });
						            $scope.showResponse("** SAML Assertion - DECODED **", xmlToJSON(b64decode(samlAssertion)));
						            console.log("DECODED SAML RESPONSE: ", removeBackslash(b64decode(samlAssertion)));
						            $scope.showResponse("** SAML Assertion - DECODED (without backslashes) **", JSON.stringify(xmlToJSON(b64decode(samlAssertion))));

						            sendToSP(samlURL, removeBackslash(b64decode(samlAssertion)));
						        }
							});
				        }
				    });

					    
					// $scope.apiResponse = JSON.stringify(response);
					// $scope.apiStatus = status;
				});
		}

		//*** Send SAML Assertion to Wordpress SAML Endpoint ***//
		var sendToSP = (actionURL, samlResponse) => {
			console.log("The action URl: ", actionURL);
			console.log("Saml res in SP: ", samlResponse);
			var requestOptions = {
				// url: actionURL,
			    follvowRedirect: true,
			    followAllRedirects: true,
			    jar: true,
			    formData: {'SAMLResponse': samlResponse}
			};
			$http.post(actionURL, requestOptions)
			.then((res) => {
				console.log('\n\n\n','* * * * * * * * * SERVICE PROVIDER RESPONSE * * * * * * * * *')
				$scope.showResponse('* * * * * * * * * SERVICE PROVIDER RESPONSE * * * * * * * * *', res);
				console.log("res: ", res);
			})
			.catch((error) => {
				console.log('\n\n\n','* * * * * * * * * SERVICE PROVIDER ERROR* * * * * * * * *')
				$scope.showResponse('* * * * * * * * * SERVICE PROVIDER ERROR * * * * * * * * *', JSON.stringify(error));
				console.log("error: ", error);
			});
		}

	}
]);
