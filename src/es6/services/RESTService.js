angular.module('login-sandbox').factory("RESTService", [
	'$q',
	'$http',
	'$cookies',
	'$ionicPlatform',
	'$timeout',
	($q, $http, $cookies, $ionicPlatform, $timeout) => {
		let HOST = 'gmp-stage.coke.com';
		let AUTH_PATH = 'https://fed.testko.com/gmpmobile';
		let BASE_PATH = `https://${HOST}/wp-json/wp/v2/`;

		class GMPRESTService {
			constructor(){
				$cookies.put('NO_CACHE', 1);
				//document.cookie = `NO_CACHE=1; domain=${HOST}; path=/`;
				//document.cookie = `NO_CACHE=1; expires=Thu, 18 Dec 2013 12:00:00 UTC; path=/`;

				//favoriteCookie = $cookies.get('myFavorite');
				  // Setting a cookie
			}

			_addCacheBuster(obj){
				obj.cb = new Date().getTime() + "_" + guid();
			}

			authenticateUser(id, pass) {
				return $q((resolve, reject) => {
					// Cancel the request after timeout of 6 sec
					// var canceller = $q.defer();
					// config.timeout = canceller.promise;

					//*** base64 encode username:password
					var basicAuth = id + ':' + pass
					var basicAuthHeader = 'Basic ' + this._b64encode(basicAuth);

					let config = {
						xhrFields: {
				    	    withCredentials: true
				    	},
				    	headers: {
				    		'Authorization': basicAuthHeader
				    		// 'Keep-Alive': 'timeout=2, max=5'
				    	}
				    	// followRedirect: true,
		    			// jar: true
				    };

				    $http.get(
					AUTH_PATH, 
					config, 
					{	
						transformResponse: function(data) {
			                // convert the data to JSON and provide
			                // it to the success function below
			                var x2js = new X2JS();
			                var json = x2js.xml_str2json( data );
			                return json;
	                    }
	                })
					.then((response, status) => {
						console.log("THE RESPONSE: ", response);
						console.log("THE STATUS: ", response.status);

						resolve(response);
					})
					.catch((error) => {
						console.log("THE ERROR: ", error);
						reject(error);
					});
					// $timeout(() => {
					// 	canceller.resolve();
					// 	reject({message: "Either the request timed out or user login is incorrect.  Test your internet connection, if connected then username and password are incorrect."});
					// }, 6000);
				});
			}

			sendAuthToSP(actionURL, samlResponse) {
				return $q((resolve, reject) => {
					console.log("The action URl: ", actionURL);
					console.log("Saml res in SP: ", samlResponse);

					// Convert response object to PHP array 
					var param = function(obj) {
				        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

			            for(name in obj) {
			                value = obj[name];

			                if(value instanceof Array) {
			                    for(i=0; i<value.length; ++i) {
			                        subValue = value[i];
			                        fullSubName = name + '[' + i + ']';
			                        innerObj = {};
			                        innerObj[fullSubName] = subValue;
			                        query += param(innerObj) + '&';
			                    }
			                } 
			                else if(value instanceof Object) {
			                    for(subName in value) {
			                        subValue = value[subName];
			                        fullSubName = name + '[' + subName + ']';
			                        innerObj = {};
			                        innerObj[fullSubName] = subValue;
			                        query += param(innerObj) + '&';
			                    }
			                } 
			                else if(value !== undefined && value !== null) query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
			            }

				        return query.length ? query.substr(0, query.length - 1) : query;
				    };

				    // Request options for the Request data
					var requestOptions = {
					    followRedirect: true,
					    followAllRedirects: true,
					    jar: true,
					    formData: {'SAMLResponse': samlResponse},
					    headers: {
					    	'Content-Type': 'application/x-www-form-urlencoded'
					    }
					};

					// Send post to Service Provider URL to receive data
					$http.post(
					// 'https://saml-tester.herokuapp.com/',  // TEST HEROKU SERVER
					actionURL,
					'SAMLResponse=' + samlResponse, 
					requestOptions,
					{
						transformRequest: function(data) {
							return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
	                    }
					})
					.then((res,err,body) => {
						console.log('\n\n\n','* * * * * * * * * SERVICE PROVIDER RESPONSE - gmpRESTService* * * * * * * * *')
						console.log("res: ", res);
						console.log("err: ", err);
						console.log("body: ", body);
						resolve(res, err, body);
					})
					.catch((error) => {
						reject(error);
					});
				});
			}

			signIn() {
				
			}
		}

		let service = new GMPRESTService();

		return service;
	}
]);
