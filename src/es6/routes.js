angular.module('login-sandbox')
.config([
	'$stateProvider',
	'$urlRouterProvider',
	'$httpProvider',
	($stateProvider, $urlRouterProvider, $httpProvider) => {

		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

		$urlRouterProvider.otherwise('/');
		
		$stateProvider.state('app', {
			url: '/',
			views: {
				'default': {
					templateUrl: 'views/home.html',
					controller: 'devController'
				}
			}
		});
	}
]);