angular.module('login-sandbox', [
	'ionic',
	'ngCordova',
	'ngCookies',
	'ngSanitize']
)

//where to route to after login
.constant('LANDING_STATE', 'app.home')
// .constant('SQLITE_DB_NAME', 'coke_gmp.db')

//.constant('ENV', 'production')
.constant('ENV', 'coke-staging')    //coke offical staging server
// .constant('ENV', 'wp-staging')    //wordpress offical staging server
//.constant('ENV', 'mock-staging')  //express.js mock staging server
//.constant('ENV', 'dev')

.run([
	'$http',
	'ENV',
	'$ionicPlatform',
	function ($http, ENV, $ionicPlatform) {
		if (ENV == 'wp-staging' || ENV == 'coke-staging' || ENV == 'production'){

			console.info(`Using ${ENV} environment, adding Authorization header to all HTTP requests`);

			/*
				CORS requires this content type, doesn't like Angular's default of, 'application/json'.

			    "The only allowed values for the Content-Type header are:
			    application/x-www-form-urlencoded
			    multipart/form-data
			    text/plain"

			    ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
			 */
			
		}
	}]
);

